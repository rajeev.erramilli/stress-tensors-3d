module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified StressTensors3d.Programs.Test
import qualified StressTensors3d.Programs.TestMixed      
import qualified StressTensors3d.Programs.TSigEps2023
import qualified StressTensors3d.Programs.TEps2023

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [ StressTensors3d.Programs.Test.boundsProgram
  , StressTensors3d.Programs.TestMixed.boundsProgram
  , StressTensors3d.Programs.TSigEps2023.boundsProgram
  , StressTensors3d.Programs.TEps2023.boundsProgram
  ]

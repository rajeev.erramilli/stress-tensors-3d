#!/bin/bash

#module load cmake/3.10.2 gcc/7.3.0 openmpi/3.0.0 boost/1_68_0-gcc730
module load cmake/3.10.2 gcc/9.2.0 boost/1_76_0_gcc-9.2.0 openmpi/4.1.1_gcc-9.2.0 eigen/eigen
echo srun /home/aliu7/install/bin/sdpb $@
srun /home/aliu7/install/bin/sdpb $@

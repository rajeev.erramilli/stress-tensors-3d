#!/bin/bash

# Default value
ntasks=32

# Set ntasks from the argument list and remove it from args if it exists
args=("$@")
for ((i=0; i<"${#args[@]}"; ++i)); do
    case ${args[i]} in
        --ntasks)
            ntasks="${args[i+1]}";
            unset args[i];
            unset args[i+1];
            break
            ;;
    esac
done

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/0.3.10-openmp ncurses slurm
ulimit -c unlimited
echo srun -n $ntasks -v /home/vdommes/install/sdpb-bigint-syrk-blas/bin/sdp2input "${args[@]}"
srun -n $ntasks -v /home/vdommes/install/sdpb-bigint-syrk-blas/bin/sdp2input "${args[@]}"

#!/bin/bash

module unload PrgEnv-intel  
module load PrgEnv-gnu  
module load eigen3/3.3.7-gnu
module load openmpi

export I_MPI_FABRICS=ofi
echo /global/homes/w/wlandry/gnu_openmpi/install/bin/sdpb $@
/global/homes/w/wlandry/gnu_openmpi/install/bin/sdpb $@

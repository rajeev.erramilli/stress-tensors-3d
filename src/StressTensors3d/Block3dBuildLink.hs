{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}

module StressTensors3d.Block3dBuildLink where

import Blocks                        (Coordinate (..))
import Blocks.Blocks3d               qualified as B3d
import Blocks.Blocks3d.BlockTableKey (BlockTableKey' (..))
import Blocks.Sign                   (toNum)
import Bootstrap.Build               (BuildLink (..))
import Bootstrap.Math.HalfInteger    (HalfInteger (..), isInteger)
import Control.Monad.IO.Class        (liftIO)
import Control.Monad.Reader          (asks, local)
import Data.List                     qualified as List
import Data.Set                      qualified as Set
import Data.Void                     (Void)
import Hyperion                      (Job, cAp, cPure, jobNodeCpus, remoteEval,
                                      setTaskCpus)
import Hyperion.Bootstrap.Bound      (BoundConfig (..), BoundFiles,
                                      SDPFetchValue, blockDir)
import Hyperion.Bootstrap.Util       (mapConcurrentlyMonitored)
import Hyperion.Util                 (minute)
import Hyperion.WorkerCpuPool        (NumCPUs (..))
import System.Directory              (doesFileExist)
import System.FilePath.Posix         ((</>))

type instance SDPFetchValue a B3d.BlockTableKey = B3d.BlockTable a

blockMinusRelativeCost :: BlockTableKey' (Set.Set HalfInteger) -> Integer
blockMinusRelativeCost key =
  - nJs * (l12 * l43 * (l12 + l43) + l12' * l43' * (l12' + l43'))
  where
    nJs = toInteger $ Set.size key.jInternal
    l halfInt
      | isInteger halfInt = (twice halfInt) `div` 2
      | otherwise = (twice halfInt + 1) `div` 2
    l' halfInt
      | isInteger halfInt = l halfInt + 1
      | otherwise = l halfInt
    l12 = l key.j12
    l43 = l key.j43
    l12' = l' key.j12
    l43' = l' key.j43

block3dBuildLink
  :: BoundConfig
  -> BoundFiles
  -> BuildLink Job B3d.BlockTableKey Void
block3dBuildLink config files = BuildLink
  { buildDeps = const []
  , checkCreated = liftIO . doesFileExist . B3d.blockTableFilePath (blockDir files)
  , buildAll = \keys -> do
      NumCPUs nodeCpus <- asks jobNodeCpus
      let
        gKeys = List.sortOn blockMinusRelativeCost $ B3d.groupBlockTableKeys keys
        (radKeys,otherKeys) = List.partition isRadialBTK gKeys
      mapConcurrentlyMonitored "Blocks3d" (2*minute) (runBlocks3d nodeCpus B3d.Debug) $
        otherKeys ++ radKeys
  }
  where
    blocks3dExecutable = scriptsDir config </> "blocks_3d.sh"

    runBlocks3d nodeCpus debugLevel t =
      local (setTaskCpus (NumCPUs cpus)) $ do
      remoteEval $ cAp (static liftIO) $
        static B3d.writeBlockTables `cAp`
        cPure blocks3dExecutable `cAp`
        cPure cpus `cAp`
        cPure debugLevel `cAp`
        cPure (blockDir files) `cAp`
        cPure t
      where
        cpus =
          let
            cpus' 
              | isRadialBTK t = 1
              | otherwise = numThreads nodeCpus t
            -- For nmax >= 10, restrict to at most 4 instances of
            -- blocks3d at once. TODO: FIXME!
            maxInstancesPerNode
              | t.lambda >= 19 = Just 4
              | otherwise      = Nothing
          in case maxInstancesPerNode of
            Nothing -> cpus'
            Just maxInstances -> max cpus' (nodeCpus `div` maxInstances)

    isRadial :: Coordinate -> Bool
    isRadial XT_Radial = True
    isRadial WS_Radial = True
    isRadial _         = False

    isRadialBTK :: BlockTableKey' (Set.Set HalfInteger) -> Bool
    isRadialBTK BlockTableKey{coordinates=coordinates} = Set.null others
      where
        (_,others) = Set.partition isRadial coordinates

    cpuMax cpus key
      | mod key.lambda 2 == 1 = min ((key.lambda+1) `div ` 2) cpus
      | otherwise = min ((key.lambda + 1 + toNum key.fourPtSign) `div` 2) cpus

    -- since I don't know the exact performance characteristics with numbers of CPUs, I'm
    -- going to approximate and say we need half as many cpus as we could use
    numThreads cpus k
      | mod c 2 == 1 = c `div` 2 + 1
      | otherwise = max 1 (c `div` 2)
      where
        c = cpuMax cpus k

{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

-- | A 'ConstantBlock' is just the constant 1, where we provide
-- instances for IsolatedBlock and ContinuumBlock so that it can be
-- used inside a 'CrossingMat'.

module StressTensors3d.ConstantBlock
  ( ConstantBlock
  , appendConstantMatrix
  ) where

import Blocks                    (BlockBase, BlockFetchContext,
                                  BlockTableParams, ContinuumBlock (..),
                                  CrossingMat, IsolatedBlock (..))
import Blocks.ScalarBlocks.Types (FourRhoCrossing (..))
import Bootstrap.Bounds          qualified as Bounds
import Bootstrap.Math.FreeVect   (vec, (*^))
import Bootstrap.Math.Linear     (toV)
import Bootstrap.Math.Linear     qualified as L
import Data.Binary               (Binary)
import Data.Functor.Compose      (Compose (..))
import Data.Matrix.Static        (Matrix)
import GHC.Generics              (Generic)
import GHC.TypeNats              (KnownNat, type (+))


data ConstantBlock = ConstantBlock
  deriving (Eq, Ord, Show, Generic, Binary)

type instance BlockFetchContext ConstantBlock a m = ()
type instance BlockTableParams ConstantBlock = ()
type instance BlockBase ConstantBlock a = FourRhoCrossing a

instance RealFloat a => IsolatedBlock ConstantBlock deriv a where
  getBlockIsolated derivs ConstantBlock = pure $ fmap (const 1) derivs

instance RealFloat a => ContinuumBlock ConstantBlock deriv a where
  getBlockContinuum _ ConstantBlock =
    error "ConstantBlock should not appear in a continuum constraint"

appendCrossingMats
  :: (KnownNat j, KnownNat n1, KnownNat n2)
  => CrossingMat j n1 b a
  -> CrossingMat j n2 b a
  -> CrossingMat j (n1 + n2) b a
appendCrossingMats m1 m2 = Compose $ fmap Compose $
  (L.++)
  <$> fmap getCompose (getCompose m1)
  <*> fmap getCompose (getCompose m2)

constantCrossingMat
  :: (Num a, Eq a)
  => Matrix j j a
  -> CrossingMat j 1 ConstantBlock a
constantCrossingMat m =
  Compose $ fmap (\c -> Compose (toV (c *^ vec ConstantBlock))) m

appendConstantMatrix
  :: forall j n b a. (KnownNat j, KnownNat n, RealFloat a, Ord b)
  => Matrix j j a
  -> CrossingMat j n b a
  -> CrossingMat j (n+1) (Either b ConstantBlock) a
appendConstantMatrix m c = appendCrossingMats
  (Bounds.mapBlocks Left c)
  (Bounds.mapBlocks Right $ constantCrossingMat m)

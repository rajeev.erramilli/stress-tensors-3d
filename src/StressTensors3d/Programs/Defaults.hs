module StressTensors3d.Programs.Defaults where

import Config qualified
import Hyperion                          (HyperionConfig (..))
import Hyperion.Bootstrap.Bound          (BoundConfig (..))
import Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..),
                                          SimplexScore (..))
import QuadraticNet                      qualified as QN
import System.FilePath.Posix             ((</>))

defaultBoundConfig :: BoundConfig
defaultBoundConfig = BoundConfig Config.scriptsDir

defaultDelaunayConfig :: Int -> Int -> DelaunayConfig
defaultDelaunayConfig nThreads nSteps = DelaunayConfig
  { simplexScore        = CandidateSeparation
  , terminateTime       = Nothing
  , nThreads            = nThreads
  , nSteps              = nSteps
  , qdelaunayExecutable = Config.scriptsDir </> "qdelaunay.sh"
  }

defaultQuadraticNetConfig :: QN.QuadraticNetConfig
defaultQuadraticNetConfig = QN.defaultQuadraticNetConfig
  (Config.scriptsDir </> "sdp2input.sh")
  (Config.scriptsDir </> "sdpb.sh")
  (dataDir Config.config </> "quadratic-net")

{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PatternSynonyms     #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}

module StressTensors3d.Programs.TSigEps2023 where

import Blocks.Blocks3d                          qualified as B3d
import Bootstrap.Bounds                         (BoundDirection (..))
import Bootstrap.Bounds.Spectrum                qualified as Spectrum
import Bootstrap.Math.AffineTransform           qualified as Affine
import Bootstrap.Math.Linear                    (pattern V2, pattern V6, toM,
                                                 toV)
import Bootstrap.Math.Util                      (dot)
import Bootstrap.Math.VectorSpace               ((*^))
import Control.Monad.IO.Class                   (liftIO)
import Control.Monad.Reader                     (asks, local, void)
import Data.Aeson                               (ToJSON)
import Data.Binary                              (Binary)
import Data.Map                                 qualified as Map
import Data.Matrix.Static                       (Matrix)
import Data.Proxy                               (Proxy (..))
import Data.Scientific                          (Scientific)
import Data.Scientific                          qualified as Scientific
import Data.Tagged                              (untag)
import Data.Text                                (Text)
import GHC.Generics                             (Generic)
import Hyperion
import Hyperion.Bootstrap.BinarySearch          (BinarySearchConfig (..),
                                                 Bracket (..))
import Hyperion.Bootstrap.Bound                 (Bound (..), CheckpointMap,
                                                 LambdaMap)
import Hyperion.Bootstrap.Bound                 qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch        (delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Main                  (unknownProgram)
import Hyperion.Bootstrap.OPESearch             (BilinearForms (..),
                                                 OPESearchConfig (..))
import Hyperion.Bootstrap.OPESearch             qualified as OPE
import Hyperion.Bootstrap.Params                qualified as Params
import Hyperion.Bootstrap.SDPDeriv              qualified as SDPDeriv
import Hyperion.Bootstrap.SDPDeriv.BFGS         qualified as BFGS
import Hyperion.Database                        qualified as DB
import Hyperion.Log                             qualified as Log
import Hyperion.Util                            (hour)
import Linear.V                                 (V)
import Numeric.Rounded                          (Rounded, RoundingMode (..))
import SDPB qualified
import StressTensors3d.Programs.Defaults        (defaultBoundConfig,
                                                 defaultDelaunayConfig,
                                                 defaultQuadraticNetConfig)
import StressTensors3d.Programs.TestMixed       (z2EvenParityEvenScalar,
                                                 z2EvenParityOddScalar,
                                                 z2OddParityEvenScalar)
import StressTensors3d.Programs.TSigEps2023Data (allowedPointsNmax14,
                                                 initialDisallowedPtsNmax10,
                                                 isingAffineNmax6,
                                                 nmax6DisallowedOPEincluded,
                                                 opeEllipseEvenTNmax6)
import StressTensors3d.TSigEps3d                (TSigEps3d (..))
import StressTensors3d.TSigEps3d                qualified as TSigEps3d
import StressTensors3d.TSigEpsSetup             (ExternalDims (..))
import StressTensors3d.TSigEpsSetup             qualified as TSigEpsSetup
import StressTensors3d.Z2Rep                    (Z2Rep (..))


z2EvenParityEvenSpin2 :: TSigEpsSetup.SymmetrySector
z2EvenParityEvenSpin2 = TSigEpsSetup.SymmetrySector 2 B3d.ParityEven Z2Even

deltaVToExternalDims :: V 2 Rational -> ExternalDims
deltaVToExternalDims (V2 deltaSig deltaEps) =
  ExternalDims { deltaSig = deltaSig, deltaEps = deltaEps }

externalDimsToDeltaV :: ExternalDims -> V 2 Rational
externalDimsToDeltaV dims = V2 dims.deltaSig dims.deltaEps

boundDeltaV :: Bound prec TSigEps3d -> V 2 Rational
boundDeltaV bound = externalDimsToDeltaV bound.boundKey.externalDims

tSigEpsNoGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsNoGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tSigEpsFeasibleDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsFeasibleDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.Feasibility mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tSigEpsNavigatorDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsNavigatorDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.NavigatorBound mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

tSigEpsSigmaNavigatorDefaultGaps :: V 2 Rational -> Maybe (V 5 Rational) -> Int -> TSigEps3d
tSigEpsSigmaNavigatorDefaultGaps deltaExt mLambdaTTT nmax = TSigEps3d
  { spectrum     = Spectrum.setGaps
    [ (z2EvenParityEvenScalar, 3 )
    , (z2OddParityEvenScalar, 3 )
    , (z2EvenParityOddScalar, 3 )
    , (z2EvenParityEvenSpin2, 4)
    ]
    $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
  , objective    = TSigEps3d.SigmaNavigatorBound mLambdaTTT
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  , externalDims = deltaVToExternalDims deltaExt
  }

remoteTSigEpsOPESearch
  :: CheckpointMap TSigEps3d
  -> LambdaMap TSigEps3d (V 5 Rational)
  -> BilinearForms 5
  -> Bound Int TSigEps3d
  -> Cluster Bool
remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (static opeSearchCfg) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg = OPESearchConfig setLambda TSigEps3d.getMatExt $
      -- It seems like quadratic net is causing some kind of crash. Turn it off for now
      -- OPE.queryAllowedMixed qmConfig
      OPE.queryAllowedDescent
      qmConfig.qmDescentConfig
      qmConfig.qmPrecision
      qmConfig.qmResolution
      qmConfig.qmHessianLineSteps
      qmConfig.qmHessianLineAverage
    setLambda lambda bound = bound
      { boundKey = bound.boundKey
        { TSigEps3d.objective = TSigEps3d.Feasibility (Just lambda) }
      }
    qmConfig = OPE.QueryMixedConfig
      { OPE.qmQuadraticNetConfig = defaultQuadraticNetConfig
      , OPE.qmDescentConfig      = OPE.defaultDescentConfig
        { OPE.maxDescentRuns = 10 }
      , OPE.qmResolution         = 1e-32
      , OPE.qmPrecision          = 384
      , OPE.qmHessianLineSteps   = 200
      , OPE.qmHessianLineAverage = 10
      }

data TSigEps3dBinarySearch = TSigEps3dBinarySearch
  { tse_bs_bound    :: Bound Int TSigEps3d
  , tse_bs_config   :: BinarySearchConfig Rational
  , tse_bs_dualPt   :: V 2 Rational
  , tse_bs_primalPt :: V 2 Rational
  } deriving (Show, Generic, Binary, ToJSON)

remoteTSigEps3dBinarySearch :: TSigEps3dBinarySearch -> Cluster (Bracket Rational)
remoteTSigEps3dBinarySearch tse_bs = Bound.remoteBinarySearchBoundWithHotStarting Bound.MkBinarySearchBound
  { Bound.bsBoundClosure      = static mkBound `cAp` cPure tse_bs.tse_bs_bound `cAp` cPure tse_bs.tse_bs_dualPt `cAp` cPure tse_bs.tse_bs_primalPt
  , Bound.bsConfig            = tse_bs.tse_bs_config
  , Bound.bsResultBoolClosure = static getBool
  }
  where
    mkBound bound dual primal fraction =
      bound { boundKey = (boundKey bound) { externalDims = deltaVToExternalDims $ (1-fraction) *^ primal + fraction *^ dual } }
    getBool _ _ = pure <$> SDPB.isPrimalFeasible

getExtMat :: Bound Int TSigEps3d -> Bound.BoundFiles -> Job (Matrix 5 5 Scientific)
getExtMat bound' files = untag @Job $
  Bound.reifyBoundWithContext bound' $ \(bound :: Bound (Proxy p) TSigEps3d) -> do
  opeMatrix <- Bound.getBoundObject bound files TSigEps3d.getMatExt
  norm <- Bound.getBoundObject bound files $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alpha <- liftIO $ SDPB.readFunctional @(Bound.BigFloat p) norm files.outDir
  pure $
    fmap Scientific.fromFloatDigits $
    fmap (alpha `dot`) opeMatrix

logExtMatLocal :: Bound Int TSigEps3d -> Bound.BoundFiles -> Cluster ()
logExtMatLocal bound files = do
  progInfo <- asks clusterProgramInfo
  extMat <- liftIO $ runJobLocal progInfo $ getExtMat bound files
  Log.info "External matrix" extMat

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

mkPointMap :: Ord v => [v] -> [v] -> Map.Map v (Maybe Bool)
mkPointMap initialAllowed initialDisallowed = Map.fromList $
  [(p, Just True)  | p <- initialAllowed] ++
  [(p, Just False) | p <- initialDisallowed]

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_test_nmax6" =
  local (setJobType (MPIJob 2 120) . setJobTime (6*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (toV (0.5181489, 1.412625),           Just lambda)
  --, (toV (0.518144590625, 1.41266015625), Just lambda)
  ]
  where
    lambda = toV
      ( (-38757737) / 88786972
      , (-82533) / 13425418
      , (-111357690) / 245817499
      , (-64848956) / 98255647
      , (-54187949) / 131974577
      )
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_Navigator_test_nmax6" =
  local (setJobType (MPIJob 12 120) . setJobTime (12*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound)
    [ (toV delta, Just $ toV lambda) | (delta, lambda) <- nmax6DisallowedOPEincluded]
  where
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14}
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_SigmaNavigator_test_nmax6" =
  local (setJobType (MPIJob 4 127) . setJobTime (24*hour) . setJobMemory "0G") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (toV delta, Just $ toV lambda) | (delta, lambda) <- nmax6DisallowedOPEincluded]
  where
    nmax = 6
    paramNmax = 8
    bound (dExt, mLambda) = Bound
      { boundKey = (tSigEpsSigmaNavigatorDefaultGaps dExt mLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "TSigEps_Navigator_BFGS_nmax6" =
  local (setJobType (MPIJob 8 120) . setJobTime (24*hour) . setJobMemory "90G" ) $
  do
    result :: (V 6 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
    bbMax = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-20
      , BFGS.stopOnNegative      = False
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5181489, 1.412625, 5115035946052249/4811183910600428, 10892257763541/727495865887682,14696384033447130/13320346099119551, 1222630505284516/760610284085429)
      , checkpointMapName = Nothing
      }
    mkBound :: V 6 Rational -> Bound Int TSigEps3d
    mkBound (V6 dSig dEps tttB tttF sse eee) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024}
      , boundConfig = defaultBoundConfig
      }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (dSig, dEps)
        lambdaInit = toV (tttB, tttF, sse, eee, 1)

boundsProgram "TSigEps_Navigator_Extremize_nmax6" =
  local (setJobType (MPIJob 8 120) . setJobTime (24*hour) . setJobMemory "90G" ) $
  do
    result :: ([V 6 Rational], BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
      SDPDeriv.remoteBFGSExtremize extConfig bjConfig directions
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
    bbMax = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
    bfgsConfig' = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-20
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    extConfig = BFGS.MkIslandExtConfig
      { BFGS.bfgsConfig = bfgsConfig'
      , BFGS.goalToleranceObj = 1e-10
      , BFGS.goalToleranceGrad = 1e-15
      , BFGS.lineSearchDecrease = 0.7
      , BFGS.maxIters = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5180, 1.411, 5115035946052249/4811183910600428, 10892257763541/727495865887682,14696384033447130/13320346099119551, 1222630505284516/760610284085429)
      , checkpointMapName = Nothing
      }
    directions = [ (V6 1 0 0 0 0 0)
                 , (V6 (-1) 0 0 0 0 0)
                 , (V6 0 1 0 0 0 0)
                 , (V6 0 (-1) 0 0 0 0)
                 ]
    mkBound :: V 6 Rational -> Bound Int TSigEps3d
    mkBound (V6 dSig dEps tttB tttF sse eee) = Bound
      { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024}
      , boundConfig = defaultBoundConfig
      }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (dSig, dEps)
        lambdaInit = toV (tttB, tttF, sse, eee, 1)

boundsProgram "TSigEps_OPEScan_test_nmax6" =
  local (setJobType (MPIJob 1 127) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

-- | Note: running this with 64 threads and 128G of memory ran into OOM issues
boundsProgram "TSigEps_Island_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 128) . setSlurmPartition "compute" . setJobTime (6*hour) . setJobMemory "0G") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    nmax = 6
    paramNmax = 6
    affine = isingAffineNmax6
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 8 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = Affine.apply affine <$> [toV (x,y) | x <- [-1,1], y <- [-1,1]]
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "TSigEps_ExtOPEBound_nmax6" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" ) $
  mapConcurrently_ (Bound.remoteComputeKeepFiles . bound)
  [ (toV (0.5181489, 1.412625),  toV ( (-39353909) / 90158408
                            , (-350635) / 56507009
                            , (-136589237) / 301491712
                            , (-104165758) / 157848095
                            , (-51410107) / 125167736 ))
  ]
  where
    nmax = 6
    paramNmax = 8
    -- precision = 2048
    bound (dExt, lambda) = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt (Just lambda) paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        , objective    = TSigEps3d.ExternalOPEBound lambda UpperBound
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024, SDPB.writeSolution = SDPB.allSolutionParts }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_test_ExternalOPEFormBound_nmax6_files_for_Petr" =
  local (setJobType (MPIJob 1 32) . setJobTime (6*hour)) $
  doConcurrently_ $ do
  lambdaForm <- lambdaForms
  let b = bound lambdaForm
  pure $ do
    (_,files) <- Bound.remoteComputeKeepFiles b
    logExtMatLocal b files
  where
    nmax = 6
    deltaExt = toV (0.5181489, 1.412625)
    lambdaForms =
      [ toM
        ( ( 0, 0, 1, 0, 0)
        , ( 0, 0, 0, 0, 0)
        , ( 1, 0, 0, 0, 0)
        , ( 0, 0, 0, 0, 0)
        , ( 0, 0, 0, 0, 0)
        )
      ]
    bound lambdaForm = Bound
      { boundKey = TSigEps3d
        { spectrum     = Spectrum.setGaps
          [ (z2EvenParityEvenScalar, 3 )
          , (z2OddParityEvenScalar, 3 )
          , (z2EvenParityOddScalar, 3 )
          ] $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum
        , objective    = TSigEps3d.ExternalOPEFormBound lambdaForm
        , blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = map fromIntegral $ Params.spinsNmax 12
        , externalDims = deltaVToExternalDims deltaExt
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_grid_test_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ toV (5181966689/10000000000, 14126542907/10000000000)
    , toV (41451765811/80000000000, 113010682093/80000000000)
    , toV (13267002881/25600000000, 180948376313/128000000000)
    , toV (10609713092177/20480000000000, 28906067058241/20480000000000)
    , toV (21222570145561/40960000000000, 57833842903433/40960000000000)
    , toV (530685745021/1024000000000, 1447619160393/1024000000000)
    , toV (21228365841927/40960000000000, 57904604103991/40960000000000)
    , toV (169812973654061/327680000000000, 463033775321053/327680000000000)
    , toV (1358568682526427/2621440000000000, 3705127289264811/2621440000000000)
    , toV (339538024043827/655360000000000, 925104881197891/655360000000000)
    , toV (169824674275653/327680000000000, 463279921698609/327680000000000)
    , toV (339602187057741/655360000000000, 925778037760893/655360000000000)
    , toV (135858502394443/262144000000000, 370540672496667/262144000000000)
    , toV (339652346194739/655360000000000, 926576487178547/655360000000000)
    , toV (84913457184483/163840000000000, 231639617077539/163840000000000)
    , toV (67915400618423/131072000000000, 185098210918983/131072000000000)
    , toV (1358383459400409/2621440000000000, 3702834299714377/2621440000000000)
    , toV (5434135901285043/10485760000000000, 14818705405476099/10485760000000000)
    , toV (169827020817161/327680000000000, 463273404357833/327680000000000)
    , toV (271686430145519/524288000000000, 29626978635863/20971520000000)
    , toV (1358310919047503/2621440000000000, 3702053474451039/2621440000000000)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 6
    paramNmax = 8
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
        , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 1024 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_peninsula_nmax6" =
  local (setJobType (MPIJob 1 128) . setJobTime (6*hour) . setJobMemory "0G" . setSlurmPartition "compute") $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ toV (x,y) | x <- [0.8,0.9..1.4], y <- [1.8,1.9..3]
  ]
  where
    nmax = 6
    paramNmax = 6
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt Nothing paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = 960, SDPB.findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_continent_binary_nmax6" =
  mapConcurrently_ search $
  [ (dualPt, toV (1,2)) | dualPt <- dualPts ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False }
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }
    lowerPts = [toV (x,1) | x <- [0.7,0.75..1]]
    leftPts = [toV (0.7,y) | y <- [1.025,1.05..2.975]]
    upperPts = [toV (x,3) | x <- [0.7,0.75..1]]
    dualPts = lowerPts ++ leftPts ++ upperPts

boundsProgram "TSigEps_continent_kink_nmax6" =
  mapConcurrently_ search $
  [ (toV (x,2.5), toV (x,1.75)) | x <- [0.8,0.81..1] ]
  where
    jobType = MPIJob 1 128
    jobTime = 6*hour
    jobMemory = "0"
    jobPartition = "compute"
    prec = 960
    nmax = 6
    search (dualPt, primalPt) =
      local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $
      remoteTSigEps3dBinarySearch $
      TSigEps3dBinarySearch
      { tse_bs_bound = Bound
        { boundKey = tSigEpsFeasibleDefaultGaps (toV (1,2)) Nothing nmax
        , precision = prec
        , solverParams = (Params.sdpbParamsNmax nmax) { SDPB.precision = prec , SDPB.findPrimalFeasible = False }
        , boundConfig = defaultBoundConfig
        }
      , tse_bs_dualPt = dualPt
      , tse_bs_primalPt = primalPt
      , tse_bs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 0
          , falsePoint = 1
          }
        , threshold  = 1e-5
        , Hyperion.Bootstrap.BinarySearch.terminateTime = Nothing
        }
      }



---------------
--- nmax=10 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "large-shared") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ( (-38757737) / 88786972
                            , (-82533) / 13425418
                            , (-111357690) / 245817499
                            , (-64848956) / 98255647
                            , (-54187949) / 131974577)
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax nmax) --{ B3d.keptPoleOrder = 14 }
      --  , spins        = Params.gnyspinsNmax 8
        }
      , precision = (Params.block3dParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 960 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "TSigEps_OPEScan_island_nmax10_higher_spin" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPoints
    (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
  where
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipseEvenTNmax6)]
    initialCheckpoint = Just $ "/expanse/lustre/scratch/dsd/temp_project/data/2024-01/jvTit/Object_3duA2V4370JJuDAobTMkWnPRKFzz29DrUZr_F1wdfFM/ck"
    initialLambda = Just $ snd $ head $ allowedPointsNmax14
    initialPoints = mkPointMap (fmap fst allowedPointsNmax14) initialDisallowedPtsNmax10
    delaunayConfig = defaultDelaunayConfig 8 200
    affine = isingAffineNmax6
    nmax = 10
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 832
        }
      , boundConfig = defaultBoundConfig
      }

---------------
--- nmax=14 ---
---------------

boundsProgram "TSigEps_OPEScan_test_nmax14" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound)
    [ -- this point should be allowed
      toV (0.5181489, 1.412625)
      --  this point should be disallowed
    -- , toV (0.518144590625, 1.41266015625)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Nothing
    initialLambda = Just $ toV ((-54537050) / 124926441,(-242728) / 39635427,(-72625868) / 160316979,(-43886811) / 66495941,(-48814271) / 118892408)
    affine = isingAffineNmax6
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = Params.jumpFindingParams paramNmax
      , boundConfig = defaultBoundConfig
      }

-- ClSWa: 6 nodes, SDPB.precision = 768
-- bxeCk: 4 nodes, SDPB.precision = 960
boundsProgram "TSigEps_OPEScan_test_nmax14_higher_spin" =
  local (setJobType (MPIJob 4 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  checkpointMap <- Bound.newCheckpointMap affine boundDeltaV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     affine boundDeltaV initialLambda
  mapConcurrently_ (remoteTSigEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound . toV)
    [ (0.5181489, 1.412625)
    , (0.5181481194, 1.412616539)
    , (0.5181484682, 1.412620912)
    , (0.5181488170, 1.412625285) -- this point is allowed
    , (0.5181485667, 1.412620903)
    , (0.5181497117, 1.412634013)
    , (0.5181493629, 1.412629640)
    , (0.5181490141, 1.412625267)
    , (0.5181492643, 1.412629649)
    ]
  where
    initialBilinearForms = BilinearForms 1e-32
      [ (Nothing, opeEllipseEvenTNmax6)
      ]
    initialCheckpoint    = Just "/expanse/lustre/scratch/dsd/temp_project/data/2023-12/bxeCk/Object_XH3Pw_9AoLt5sCvhH1NZ2edDG6uMhxV-Pmsan3_ra-Q/ck_bak"
    initialLambda = Just $ toV
      ( (-71661766) / 164153725
      , (-492853) / 80477224
      , (-18114093) / 39985732
      , (-93947475) / 142346144
      , (-44275837) / 107838670
      )
    affine = isingAffineNmax6
    nmax = 14
    paramNmax = nmax+4
    bound dExt = Bound
      { boundKey = (tSigEpsFeasibleDefaultGaps dExt initialLambda paramNmax)
        { blockParams  = (Params.block3dParamsNmax paramNmax) { B3d.nmax=nmax }
        , spins = Params.gnyspinsNmax paramNmax
        }
      , precision = (Params.block3dParamsNmax paramNmax).precision
      , solverParams = (Params.jumpFindingParams paramNmax)
        { SDPB.verbosity = 2
        , SDPB.precision = 960
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram p = unknownProgram p

{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DefaultSignatures    #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DerivingStrategies   #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE MultiWayIf           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TSig3d
  ( tsBuildChain
  , tsFetchConfig
  , derivsVecTSig3d
  , TSig3d(..)
  , Objective(..)
  ) where

import Blocks                           (BlockFetchContext, Coordinate (XT),
                                         CrossingMat, Delta (..),
                                         Derivative (..), TaylorCoeff (..),
                                         Taylors, runTagged)
import Blocks.Blocks3d                  (ConformalRep (..), Parity (..))
import Blocks.Blocks3d                  qualified as B3d
import Bootstrap.Bounds                 (BoundDirection (..),
                                         FourPointFunctionTerm, OPECoefficient,
                                         OPECoefficientExternal, Spectrum,
                                         addOPECoeffExt, boundDirSign,
                                         crossingMatrix, crossingMatrixExternal,
                                         derivsVec, mapBlocks)
import Bootstrap.Bounds                 qualified as Bounds
import Bootstrap.Build                  (BuildChain, FChain (..),
                                         FetchConfig (..), SomeBuildChain (..),
                                         SumDropVoid, addBuildChain, noDeps)
import Bootstrap.Math.FreeVect          (FreeVect)
import Bootstrap.Math.HalfInteger       (HalfInteger)
import Bootstrap.Math.Linear            (bilinearPair, toV)
import Bootstrap.Math.VectorSpace       (zero, (*^))
import Control.Monad.IO.Class           (MonadIO, liftIO)
import Data.Aeson                       (FromJSON, ToJSON)
import Data.Binary                      (Binary)
import Data.Data                        (Typeable)
import Data.Maybe                       (maybeToList)
import Data.Reflection                  (Reifies)
import Data.Tagged                      (Tagged)
import Data.Vector                      (Vector)
import GHC.Generics                     (Generic)
import GHC.TypeNats                     (KnownNat)
import Hyperion                         (Dict (..), Job, Static (..))
import Hyperion.Bootstrap.Bound         (BigFloat, BoundConfig, BoundFiles,
                                         BuildInJob, SDPFetchBuildConfig (..),
                                         ToKeyVals, ToSDP (..), blockDir,
                                         reifyNatFromInt)
import Linear.V                         (V)
import SDPB qualified
import StressTensors3d.Block3dBuildLink (block3dBuildLink)
import StressTensors3d.CompositeBlock   (CompositeBlockKey,
                                         compositeBlockBuildLink,
                                         readCompositeBlockTable)
import StressTensors3d.CrossingEqs      (crossingEqsStructSet, structsSSSS,
                                         structsTTSS, structsTTTT)
import StressTensors3d.TSigEpsSetup     (ExternalDims (..), ExternalOp (..),
                                         SymmetrySector (..), TSMixedBlock,
                                         TSMixedStruct, TSMixedStructLabel (..),
                                         lambda_B, lambda_F, listSpectrum,
                                         opeSS, opeTS, opeTT, toTSMixedBlock,
                                         wardSST)
import StressTensors3d.TTOStructLabel   (TTOStructLabel (..))
import StressTensors3d.TTTT3d           (withNonEmptyVec)
import StressTensors3d.Z2Rep            (Z2Rep (..))

data TSig3d = TSig3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound (V 2 Rational) BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type TSigNumCrossing = 28

crossingEqs
  :: forall s a b . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TSigNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,Sig,Sig),     structsTTSS)
  , ((Sig,T,Sig,T),     structsTTSS)
  , ((Sig,Sig,Sig,Sig), structsSSSS)
  ]

derivsVecTSig3d :: TSig3d -> V TSigNumCrossing (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecTSig3d f = derivsVec crossingEqs <*> pure f.blockParams.nmax

ope
  :: (Fractional a, Eq a, Reifies s ExternalDims)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> opeTT r parity ++ opeSS Sig r parity
  Z2Odd  -> opeTS Sig r parity

matUnit
  :: forall s a . (Reifies s ExternalDims , Fractional a, Eq a)
  => Tagged s (CrossingMat 1 TSigNumCrossing B3d.IdentityBlock3d a)
matUnit = pure $ B3d.identityCrossingMat (crossingEqs @s)

opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s ExternalDims)
  => V 2 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( lambda_B `addOPECoeffExt` wardSST Sig
  , lambda_F `addOPECoeffExt` wardSST Sig
  )

matExt
  :: forall a s . (Fractional a, Eq a,  Reifies s ExternalDims)
  => Tagged s (CrossingMat 2 TSigNumCrossing TSMixedBlock a)
matExt =
  pure $ mapBlocks toTSMixedBlock $
  crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Sig]

bulkConstraints
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext TSMixedBlock a m, Reifies s ExternalDims)
  => TSig3d
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints t = pure $ do
  (delta, range, SymmetrySector j parity z2) <- listSpectrum t.spins t.spectrum
  let
    opeCoeffs = ope (ConformalRep delta j) parity z2
    params = (t.blockParams, (t.blockParams, t.blockParams))
  maybeToList $ withNonEmptyVec opeCoeffs $ \opeCoeffs' ->
    Bounds.bootstrapConstraint params (derivsVecTSig3d t) range $
    mapBlocks toTSMixedBlock $
    crossingMatrix opeCoeffs' (crossingEqs @s)

tSig3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m, BlockFetchContext TSMixedBlock a m)
  => TSig3d
  -> SDPB.SDP m a
tSig3dSDP t = runTagged t.externalDims $ do
  bulk <- bulkConstraints t
  unit <- matUnit
  ext <- matExt
  let
    dv = derivsVecTSig3d t
    params = (t.blockParams, (t.blockParams, t.blockParams))
    (cons, obj, norm) = case t.objective of
      Feasibility mLambda ->
        let
          stressConstraint = case mLambda of
            Just lambda -> Bounds.isolatedConstraint params dv $
              bilinearPair (fmap fromRational lambda) ext
            Nothing -> Bounds.isolatedConstraint params dv ext
        in
          ( bulk ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dv $ zero `asTypeOf` unit
          , Bounds.bootstrapNormalization t.blockParams dv unit
          )
      StressTensorOPEBound lambda direction ->
        let
          matLambda = boundDirSign direction *^ bilinearPair (fmap fromRational lambda) ext
        in
          ( bulk
          , Bounds.bootstrapObjective t.blockParams dv unit
          , Bounds.bootstrapNormalization params dv matLambda
          )
  return $ SDPB.SDP obj norm cons

instance ToSDP TSig3d where
  type SDPFetchKeys TSig3d = '[ B3d.BlockTableKey
                              , CompositeBlockKey TTOStructLabel
                              , CompositeBlockKey TSMixedStructLabel ]
  toSDP = tSig3dSDP
  reifyPrecisionWithFetchContext _ _ n go = reifyNatFromInt n go

tsFetchConfig
  :: (KnownNat p, MonadIO m)
  => BoundFiles
  -> FetchConfig m (ToKeyVals (BigFloat p) (SDPFetchKeys TSig3d))
tsFetchConfig boundFiles =
  liftIO . B3d.readBlockTable (blockDir boundFiles) :&:
  readCompositeBlockTable boundFiles.blockDir :&:
  readCompositeBlockTable boundFiles.blockDir :&:
  FetchNil

-- TODO: Right now, the pure Block3d's are computed at the same
-- stage as the CompositeBlocks, before the CompositeBlocks. We might
-- consider changing this: for example, the pure Block3d's could be
-- computed at the earlier stage where the dependencies of the
-- CompositeBlocks are built, or we could do things in a different
-- order.
tsBuildChain
  :: BoundConfig
  -> BoundFiles
  -> SomeBuildChain Job (SumDropVoid (SDPFetchKeys TSig3d))
tsBuildChain bConfig boundFiles =
  SomeBuildChain $
  addBuildChain (>>) b3dChain $
  addBuildChain (>>) ttttBlockChain generalBlockChain
  where
    generalBlockChain :: BuildChain Job '[CompositeBlockKey TSMixedStructLabel, B3d.BlockTableKey]
    generalBlockChain =
      compositeBlockBuildLink boundFiles.blockDir :< b3dChain

    ttttBlockChain :: BuildChain Job '[CompositeBlockKey TTOStructLabel, B3d.BlockTableKey]
    ttttBlockChain =
      compositeBlockBuildLink boundFiles.blockDir :< b3dChain

    b3dChain :: BuildChain Job '[B3d.BlockTableKey]
    b3dChain = noDeps $ block3dBuildLink bConfig boundFiles

instance SDPFetchBuildConfig TSig3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TSig3d)              where closureDict = static Dict
instance Static (Show TSig3d)                where closureDict = static Dict
instance Static (ToSDP TSig3d)               where closureDict = static Dict
instance Static (ToJSON TSig3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TSig3d) where closureDict = static Dict
instance Static (BuildInJob TSig3d)          where closureDict = static Dict

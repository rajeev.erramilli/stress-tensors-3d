{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module StressTensors3d.TTTT3d where

import Blocks                           (BlockFetchContext, Coordinate (XT),
                                         CrossingMat, Delta (..),
                                         Derivative (..), TaylorCoeff (..),
                                         Taylors)
import Blocks.Blocks3d                  (ConformalRep (..), Parity (..))
import Blocks.Blocks3d                  qualified as B3d
import Bootstrap.Bounds                 (FourPointFunctionTerm, HasRep (..),
                                         OPECoefficient, OPECoefficientExternal,
                                         Spectrum, crossingMatrix,
                                         crossingMatrixExternal, derivsVec,
                                         listDeltas, mapBlocks,
                                         opeCoeffExternalSimple,
                                         opeCoeffIdentical_)
import Bootstrap.Bounds                 qualified as Bounds
import Bootstrap.Bounds.BoundDirection  (BoundDirection (..), boundDirSign)
import Bootstrap.Build                  (FChain (..), FetchConfig (..),
                                         SomeBuildChain (..), noDeps)
import Bootstrap.Math.FreeVect          (FreeVect, vec)
import Bootstrap.Math.HalfInteger       (HalfInteger, pattern AnInteger,
                                         pattern EvenInteger)
import Bootstrap.Math.Linear            (bilinearPair, toV)
import Bootstrap.Math.VectorSpace       (zero, (*^), (^+^))
import Config qualified
import Data.Aeson                       (FromJSON, FromJSONKey, ToJSON,
                                         ToJSONKey)
import Data.Binary                      (Binary)
import Data.Data                        (Typeable)
import Data.Maybe                       (maybeToList)
import Data.Vector                      (Vector)
import Data.Vector                      qualified as Vector
import GHC.Generics                     (Generic)
import GHC.TypeNats                     (KnownNat)
import Hyperion                         (Dict (..), HyperionConfig (..),
                                         Static (..))
import Hyperion.Bootstrap.Bound         (BuildInJob, SDPFetchBuildConfig (..),
                                         ToSDP (..), blockDir)
import Hyperion.Bootstrap.Util          (ToPath (..), dirScatteredHashBasePath)
import Linear.V                         (V, reifyVectorNat)
import SDPB qualified
import StressTensors3d.Block3dBuildLink (block3dBuildLink)
import StressTensors3d.CompositeBlock   (CompositeBlock (..), CompositeBlockKey,
                                         compositeBlockBuildLink,
                                         readCompositeBlockTable)
import StressTensors3d.CrossingEqs      (HasT (..), crossingEqsStructSet,
                                         structsTTTT)
import StressTensors3d.ThreePtStruct    (ThreePtStruct)
import StressTensors3d.TTOStructLabel   (TTOStructLabel (..))
import System.FilePath.Posix            ((<.>), (</>))

-------------------- Setup ---------------------

data ExternalOp = T
  deriving (Show, Eq, Ord, Enum, Bounded)

instance HasT ExternalOp where
  stressTensor = T

instance HasRep ExternalOp (ConformalRep Rational) where
  rep T = ConformalRep 3 2

data SymmetrySector = SymmetrySector HalfInteger Parity
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

instance Static (Binary SymmetrySector) where
  closureDict = static Dict

data TTTT3d = TTTT3d
  { spectrum    :: Spectrum SymmetrySector
  , objective   :: Objective
  , spins       :: [HalfInteger]
  , blockParams :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound (V 2 Rational) BoundDirection
  | FeasibilityFixedOPE (V 2 Rational)
  | FeasibilityLowerBoundOPE (V 2 Rational)
  | TTeOPEBound Rational BoundDirection
  | TTeOPEBoundWithLambda Rational BoundDirection (V 2 Rational)
  | TTsOPEBound Rational BoundDirection
  -- | CTBound BoundDirection -- | convention is that UpperBound is a cT upper bound
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type TTOStruct = ThreePtStruct Delta TTOStructLabel
type TTTTBlock = CompositeBlock TTOStructLabel

-------------------- Crossing equations and OPEs ---------------------

type TTTTNumCrossing = 18

-- | Crossing equations for a four-point function of 3d stress tensors
crossingEqsTTTT3d
  :: forall a b . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm ExternalOp B3d.Q4Struct b a
  -> V TTTTNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqsTTTT3d = crossingEqsStructSet [((T,T,T,T), structsTTTT)]

derivsVecTTTT3d :: TTTT3d -> V TTTTNumCrossing (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecTTTT3d f =
  fmap ($ (blockParams f).nmax) (derivsVec crossingEqsTTTT3d)

crossingMatTTTT3d
  :: forall j a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient ExternalOp TTOStruct a)
  -> (CrossingMat j TTTTNumCrossing TTTTBlock a)
crossingMatTTTT3d ope =
  mapBlocks CompositeBlock $
  crossingMatrix ope crossingEqsTTTT3d

matTTUnit :: (Fractional a, Eq a) => CrossingMat 1 TTTTNumCrossing B3d.IdentityBlock3d a
matTTUnit = B3d.identityCrossingMat crossingEqsTTTT3d

opeTTT :: (Eq a, Fractional a) => V 2 (OPECoefficientExternal ExternalOp TTOStruct a)
opeTTT =
  fmap (opeCoeffExternalSimple T T T . vec) $
  toV (StressTensorB, StressTensorF)

matTTT :: (Fractional a, Eq a) => CrossingMat 2 TTTTNumCrossing TTTTBlock a
matTTT =
  mapBlocks CompositeBlock $
  crossingMatrixExternal opeTTT crossingEqsTTTT3d [T]

opeTT
  :: (Fractional a, Eq a)
  => ConformalRep Delta
  -> Parity
  -> [OPECoefficient ExternalOp TTOStruct a]
opeTT r@(ConformalRep _ j) parity =
  map (opeCoeffIdentical_ T r . vec) $
  case (parity, j) of
    (ParityEven, 0)                    -> [ScalarParityEven]
    (ParityOdd,  0)                    -> [ScalarParityOdd]
    (ParityEven, 2)                    -> [Spin2ParityEven]
    (ParityOdd,  2)                    -> [Spin2ParityOdd]
    (ParityEven, EvenInteger l)        -> [GenericParityEven1 l, GenericParityEven2 l]
    (ParityOdd,  AnInteger l) | l >= 4 -> [GenericParityOdd l]
    _                                  -> []

matTTeven :: (Fractional a, Eq a) => Rational -> CrossingMat 1 TTTTNumCrossing TTTTBlock a
matTTeven d = crossingMatTTTT3d (toV opeTTeven)
  where
    delta = Fixed d
    scalarRep = B3d.ConformalRep delta 0
    opeTTeven = head $ opeTT scalarRep ParityEven

matTTodd :: (Fractional a, Eq a) => Rational -> CrossingMat 1 TTTTNumCrossing TTTTBlock a
matTTodd d = crossingMatTTTT3d (toV opeTTodd)
  where
    delta = Fixed d
    scalarRep = B3d.ConformalRep delta 0
    opeTTodd = head $ opeTT scalarRep ParityOdd

-------------------- SDP Definition ---------------------

withNonEmptyVec :: [a] -> (forall n . KnownNat n => V n a -> r) -> Maybe r
withNonEmptyVec [] _  = Nothing
withNonEmptyVec xs go = Just $ reifyVectorNat (Vector.fromList xs) go

tttt3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m, BlockFetchContext TTTTBlock a m)
  => TTTT3d
  -> SDPB.SDP m a
tttt3dSDP t =
  let
    dvTT = derivsVecTTTT3d t

    bulkConstraints = do
      parity <- [ParityEven, ParityOdd]
      j <- t.spins
      (delta, range) <- listDeltas (SymmetrySector j parity) t.spectrum
      let ope = opeTT (ConformalRep delta j) parity
      maybeToList $ withNonEmptyVec ope $ \ope' ->
        Bounds.bootstrapConstraint t.blockParams dvTT range $ crossingMatTTTT3d ope'

    (cons, obj, norm) = case t.objective of
      Feasibility mLambdaTTT ->
        let
          stressConstraint = case mLambdaTTT of
            Just lambdaTTT -> Bounds.isolatedConstraint t.blockParams dvTT $
              bilinearPair (fmap fromRational lambdaTTT) matTTT
            Nothing -> Bounds.isolatedConstraint t.blockParams dvTT matTTT
        in
          ( bulkConstraints ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dvTT $ zero `asTypeOf` matTTUnit
          , Bounds.bootstrapNormalization t.blockParams dvTT matTTUnit
          )
      StressTensorOPEBound lambdaTTT direction ->
        let
          matLambdaTTT = boundDirSign direction *^ bilinearPair (fmap fromRational lambdaTTT) matTTT
        in
          ( bulkConstraints
          , Bounds.bootstrapObjective t.blockParams dvTT matTTUnit
          , Bounds.bootstrapNormalization t.blockParams dvTT matLambdaTTT
          )
      TTeOPEBound deltaEven direction ->
        let
          stressConstraint = Bounds.isolatedConstraint t.blockParams dvTT matTTT
          matLambdaTTe = boundDirSign direction *^ (matTTeven deltaEven)
        in
          ( bulkConstraints ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dvTT matTTUnit
          , Bounds.bootstrapNormalization t.blockParams dvTT matLambdaTTe
          )
      TTeOPEBoundWithLambda deltaEven direction lambdaTTT ->
        let
          stressConstraint = Bounds.isolatedConstraint t.blockParams dvTT $
              bilinearPair (fmap fromRational lambdaTTT) matTTT
          matLambdaTTe = boundDirSign direction *^ (matTTeven deltaEven)
        in
          ( bulkConstraints ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dvTT matTTUnit
          , Bounds.bootstrapNormalization t.blockParams dvTT matLambdaTTe
          )
      TTsOPEBound deltaOdd direction ->
        let
          stressConstraint = Bounds.isolatedConstraint t.blockParams dvTT matTTT
          matLambdaTTs = boundDirSign direction *^ (matTTodd deltaOdd)
        in
          ( bulkConstraints ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dvTT matTTUnit
          , Bounds.bootstrapNormalization t.blockParams dvTT matLambdaTTs
          )
      FeasibilityFixedOPE lambdaTTT ->
        let
          stress = mapBlocks Left $ bilinearPair (fmap fromRational lambdaTTT) matTTT
          stressPlusId = stress ^+^ mapBlocks Right matTTUnit
        in
          ( bulkConstraints
          , Bounds.bootstrapObjective t.blockParams dvTT $ zero `asTypeOf` matTTUnit
          , Bounds.bootstrapNormalization (t.blockParams, t.blockParams) dvTT stressPlusId
          )
      FeasibilityLowerBoundOPE lambdaTTT ->
        let
          stress = mapBlocks Left $ bilinearPair (fmap fromRational lambdaTTT) matTTT
          stressPlusId = stress ^+^ mapBlocks Right matTTUnit
          stressConstraint = Bounds.isolatedConstraint t.blockParams dvTT matTTT
        in
          ( bulkConstraints ++ [stressConstraint]
          , Bounds.bootstrapObjective t.blockParams dvTT $ zero `asTypeOf` matTTUnit
          , Bounds.bootstrapNormalization (t.blockParams, t.blockParams) dvTT stressPlusId
          )
  in
    SDPB.SDP obj norm cons

instance ToSDP TTTT3d where
  type SDPFetchKeys TTTT3d = '[ CompositeBlockKey TTOStructLabel ]
  toSDP = tttt3dSDP

instance SDPFetchBuildConfig TTTT3d where
  sdpFetchConfig _ _ cftBoundFiles =
    readCompositeBlockTable cftBoundFiles.blockDir :&:
    FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $
    (compositeBlockBuildLink cftBoundFiles.blockDir) :<
    (noDeps $ block3dBuildLink bConfig cftBoundFiles)

instance Static (Binary TTTT3d)              where closureDict = static Dict
instance Static (Show TTTT3d)                where closureDict = static Dict
instance Static (ToSDP TTTT3d)               where closureDict = static Dict
instance Static (ToJSON TTTT3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TTTT3d) where closureDict = static Dict
instance Static (BuildInJob TTTT3d)          where closureDict = static Dict

----------- Block files -------------

ttttBlockDir :: FilePath
ttttBlockDir = Config.config.dataDir </> "tttt_blocks2"

-- | Ensure tttt blocks are placed in a central location by defining a
-- custom ToPath instance
instance ToPath (CompositeBlockKey TTOStructLabel) where
  toPath _ k =
    ttttBlockDir </> dirScatteredHashBasePath "tttt_block" k <.> "dat"

instance Static (ToPath (CompositeBlockKey TTOStructLabel)) where
  closureDict = static Dict
